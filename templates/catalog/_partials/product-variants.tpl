<div class="product-variants product-quantity">
  {foreach from=$groups key=id_attribute_group item=group}
    {if !empty($group.attributes)}
      <div class="clearfix product-variants-item sm mr-2">
        <span class="control-label">{$group.name}</span>
        {if $group.group_type == 'select'}
          <select
                  class="form-control form-control-select"
                  id="group_{$id_attribute_group}"
                  data-product-attribute="{$id_attribute_group}"
                  name="group[{$id_attribute_group}]">
            {foreach from=$group.attributes key=id_attribute item=group_attribute}
              <option value="{$id_attribute}"
                      title="{$group_attribute.name}"{if $group_attribute.selected} selected="selected"{/if}>{$group_attribute.name}</option>
            {/foreach}
          </select>
        {elseif $group.group_type == 'color'}
          <ul id="group_{$id_attribute_group}">
            {foreach from=$group.attributes key=id_attribute item=group_attribute}
              <li class="float-xs-left input-container">
                <label aria-label="{$group_attribute.name}">
                  <input class="input-color" type="radio" data-product-attribute="{$id_attribute_group}"
                         name="group[{$id_attribute_group}]" value="{$id_attribute}"
                         title="{$group_attribute.name}"{if $group_attribute.selected} checked="checked"{/if}>
                  <span
                  {if $group_attribute.texture}
                    class="color texture" style="background-image: url({$group_attribute.texture})"
                  {elseif $group_attribute.html_color_code}
                    class="color" style="background-color: {$group_attribute.html_color_code}"
                  {/if}
                ><span class="sr-only">{$group_attribute.name}</span></span>
                </label>
              </li>
            {/foreach}
          </ul>
        {elseif $group.group_type == 'radio'}
          <ul id="group_{$id_attribute_group}">
            {foreach from=$group.attributes key=id_attribute item=group_attribute}
              <li class="input-container float-xs-left">
                <label>
                  <input class="input-radio" type="radio" data-product-attribute="{$id_attribute_group}"
                         name="group[{$id_attribute_group}]" value="{$id_attribute}"
                         title="{$group_attribute.name}"{if $group_attribute.selected} checked="checked"{/if}>
                  <span class="radio-label">{$group_attribute.name}</span>
                </label>
              </li>
            {/foreach}
          </ul>
        {/if}
      </div>
    {/if}
  {/foreach}
  <div class="product-variants-item">
    <div>
      <span class="control-label mr-2">{l s='Quantity' d='Shop.Theme.Catalog'}</span>
    </div>
    <div class="qty">
      <input
              type="number"
              name="qty"
              id="quantity_wanted"
              {if $product.quantity_wanted}
                value="{$product.quantity_wanted}"
                min="{$product.minimal_quantity}"
              {else}
                value="1"
                min="1"
              {/if}
              class="input-group"
              aria-label="{l s='Quantity' d='Shop.Theme.Actions'}"
      >
    </div>
    {hook h='displayProductActions' product=$product}
  </div>
</div>
